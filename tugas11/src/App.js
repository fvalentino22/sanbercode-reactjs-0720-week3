import React from 'react';
//import logo from './logo.svg';
import './App.css';
//import TableBuah from './tugas11/SetupInfo';
//import Timer from './tugas12/Timer';
//import Timer from './tugas12/CountDown';
//import Clock from './tugas12/Jam';
import Table from './tugas13/table';
import { BrowserRouter as Router} from "react-router-dom";
import Routes from './tugas15/Routes'
function App() {
  return (
    <div /*className="App"*/ >
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      {/* <TableBuah /> */}
      {/* <Timer/> */}
      <Router>
        <Routes/>
      </Router>
      {/* <Table/> */}
    </div>
  );
}

export default App;
