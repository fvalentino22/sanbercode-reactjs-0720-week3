import React, { Component } from 'react'
//import './table.css';

class Table extends Component {
    constructor(props) {
        super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
        this.state = {}
        this.state.filterText = ""
        this.state.products = [ //state is by default an object
            
                {nama: "Semangka", harga: 10000, berat: '1 kg'},
                {nama: "Anggur", harga: 40000, berat: '0.5 kg'},
                {nama: "Strawberry", harga: 30000, berat: '0.4 kg'},
                {nama: "Jeruk", harga: 30000, berat: '1 kg'},
                {nama: "Mangga", harga: 30000, berat: '0.5 kg'}
            ]
          }
    
    // renderTableHeader() {
    //     let header = Object.keys(this.state.dataHargaBuah[0])
    //     return header.map((key, index) => {
    //        return <th key={index}>{key[0].toUpperCase()+key.substr(1)}</th>
    //     })
    // }

    // renderTableData() {
    //     return this.state.dataHargaBuah.map((dataHargaBuah, index) => {
    //         const { nama, harga, berat } = dataHargaBuah //destructuring
    //         return (
    //             <tr key={nama}>
    //                 <td>{nama}</td>
    //                 <td>{harga}</td>
    //                 <td>{berat}</td>
    //             </tr>
    //         )
    //     })
    // }

    // Tambahan dari internet.js
    //products = dataHargaBuah
    
    handleUserInput(filterText) {
        this.setState({filterText: filterText});
      };
      
      handleRowDel(product) {
        var index = this.state.products.indexOf(product);
        this.state.products.splice(index, 1);
        this.setState(this.state.products);
      };
    
      handleAddEvent(evt) {
        var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
        var product = {
          id:id,
          nama: "",
          harga: "",
          berat: ""
        
        }
        this.state.products.push(product);
        this.setState(this.state.products);
      }

      handleProductTable(evt) {
        var item = {
          id: evt.target.id,
          name: evt.target.name,
          value: evt.target.value
        };
        var products = this.state.products.slice();
        var newProducts = products.map(function(product) {
            for (var key in product) {
                if (key === item.name && product.id === item.id) {
                    product[key] = item.value;
                }
            }
            return product;
        });
        this.setState({products:newProducts});
      //  console.log(this.state.products);
      };

    render() {
        return (
            <>
            <SearchBar filterText={this.state.filterText} onUserInput={this.handleUserInput.bind(this)}/>
            <ProductTable onProductTableUpdate={this.handleProductTable.bind(this)} onRowAdd={this.handleAddEvent.bind(this)} onRowDel={this.handleRowDel.bind(this)} products={this.state.products} filterText={this.state.filterText}/>
                {/* <h1 id='title'>Tabel Harga Buah</h1>
                <table id='dataHargaBuah'>
                    <tbody>
                        <tr>{this.renderTableHeader()}</tr>
                        {this.renderTableData()}
                    </tbody>
                </table> */}
                
            </>
        )
    }
}
class SearchBar extends React.Component {
    handleChange() {
      this.props.onUserInput(this.refs.filterTextInput.value);
    }
    render() {
      return (
        <div>
  
          <input type="text" placeholder="Search..." value={this.props.filterText} ref="filterTextInput" onChange={this.handleChange.bind(this)}/>
  
        </div>
  
      );
    }
}

class ProductTable extends React.Component {

  render() {
    var onProductTableUpdate = this.props.onProductTableUpdate;
    var rowDel = this.props.onRowDel;
    var filterText = this.props.filterText;
    var product = this.props.products.map(function(product) {
      if (product.nama.indexOf(filterText) === -1) {
        //return;
      }
      return (<ProductRow onProductTableUpdate={onProductTableUpdate} product={product} onDelEvent={rowDel.bind(this)} key={product.id}/>)
    });
    return (
      <div>


      <button type="button" onClick={this.props.onRowAdd} className="btn btn-success pull-right">Add</button>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
          </thead>

          <tbody>
            {product}

          </tbody>

        </table>
      </div>
    );

  }

}
  class ProductRow extends React.Component {
    onDelEvent() {
      this.props.onDelEvent(this.props.product);
  
    }
    render() {
  
      return (
        <tr className="eachRow">
          <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
            "type": "nama",
            value: this.props.product.nama,
            id: this.props.product.id
          }}/>
          <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
            type: "harga",
            value: this.props.product.harga,
            id: this.props.product.id
          }}/>
          <EditableCell onProductTableUpdate={this.props.onProductTableUpdate} cellData={{
            type: "berat",
            value: this.props.product.berat,
            id: this.props.product.id
          }}/>
          <td className="del-cell">
            <input type="button" onClick={this.onDelEvent.bind(this)} value="X" className="del-btn"/>
          </td>
        </tr>
      );
  
    }
  
  }

  class EditableCell extends React.Component {

    render() {
      return (
        <td>
          <input type='text' name={this.props.cellData.type} id={this.props.cellData.id} value={this.props.cellData.value} onChange={this.props.onProductTableUpdate}/>
        </td>
      );
  
    }
  
  }

export default Table