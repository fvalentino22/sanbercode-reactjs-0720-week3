import React from 'react';


class Judul extends React.Component{
    render(){
        return <h1 style={{textAlign:'center'}}>Tabel Harga Buah</h1>
    }
}
let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

  class TableHeader extends React.Component{
    render() {
        return (
        <>
            <table className = 'Table-header' style={{width:'100%'}} >
                <thead>
                    <tr>
                        <th style={{border:'2px solid white',backgroundColor:'grey'}}>Nama</th>
                        <th style={{border:'2px solid white',backgroundColor:'grey'}}>Harga</th>
                        <th style={{border:'2px solid white',backgroundColor:'grey'}}>Berat</th>
                     
                    </tr>
                </thead>
            </table>
        </>
        )
    }
  }
    class TableContent extends React.Component {
    render() {
      return (
          
        <>
          {dataHargaBuah.map(el=> {
            return (                  
            <table className = 'Table-content' style={{border:'2px solid white', width:'100%'}}>
                <tbody>
                    <tr style={{border:'2px solid white',backgroundColor:'pink'}}>
                        <td>{el.nama}</td>
                        <td>{el.harga}</td>
                        <td>{el.berat}</td>
                    </tr>
                </tbody>
                  
                  {/* style={{border: "1px solid #000", padding: "20px"}}> */}
                {/* <Welcome name={el.name}/> 
                <ShowAge age={el.age}/>  */}
              </table>
            )
          })}
        </>
      )
    }
}

class TableBuah extends React.Component {
    render() {
        return(
            <div className = 'Table' style={{border:'1px solid black', padding:'50px'}}>
                <Judul/>
                <TableHeader/>
                <TableContent/>
                
                
            </div>
        )
    }
}
export default TableBuah