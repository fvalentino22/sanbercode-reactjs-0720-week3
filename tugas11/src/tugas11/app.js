import React from 'react';
import './App.css';
import UserInfo from './UserInfo';

function App() {
  return (
    <div>
      <UserInfo />
    </div>
  );
}

export default App;