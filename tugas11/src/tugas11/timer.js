import React, { Component } from 'react'

class Countdown extends Component {
    state = {
        countdown: 3,
        time: new Date()
    }

    componentDidMount() {
        this.myInterval = setInterval(() => {
            const { countdown } = this.state
            if (countdown === 0) {
                clearInterval(this.myInterval)
            } else {
                this.setState(({ countdown }) => ({
                    countdown: countdown - 1
                }))
            }
        }, 1000)
        this.intervalID = setInterval(
            () => this.tick(),
            1000
          );
    }

    componentWillUnmount() {
        clearInterval(this.myInterval)
        clearInterval(this.intervalID);
    }

    tick() {
        this.setState({
          time: new Date()
     });
    }

    render() {
        const { countdown } = this.state
        return (
            <div>
                { countdown === 0
                    ? null
                    : <h1>Sekarang jam {this.state.time.toLocaleTimeString()} - hitung mundur: {countdown}</h1>
                }
            </div>
        )
    }
}

export default Countdown