import React from 'react';


class Clock extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        time: new Date()
      };
    }
    componentDidMount() {
      this.intervalID = setInterval(
        () => this.tick(),
        1000
      );
    }
    componentWillUnmount() {
      clearInterval(this.intervalID);
    }
    tick() {
      this.setState({
        time: new Date()
      });
    }
    render() {
      return ( 
      <div>
        <p className="App-clock" style={{textAlign:'center'}}>
          Sekarang jam {this.state.time.toLocaleTimeString()}.
        </p>
        </div>
      );
    }
  }

export default Clock;