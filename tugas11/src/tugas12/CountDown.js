import React, {Component} from 'react'
import Clock from './Jam'
//import Table from 'D:/PELATIHAN SANBERCODE/sanbercode-reactjs-0720-week3/tugas11/src/tugas11/table'

class Timer extends Component {
    state = {
        seconds: 100,
    }

    componentDidMount() {
        this.myInterval = setInterval(() => {
            const { seconds } = this.state

            if (seconds > 0) {
                this.setState(({ seconds }) => ({
                    seconds: seconds - 1
                }))
            }
            else if (seconds === 0) {
                    clearInterval(this.myInterval)
                }
                }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.myInterval)
    }

    render() {
        const { seconds } = this.state
        return (
            <div>
                { seconds === 0 ? null:
                <h1 style={{textAlign:'center'}}><Clock/>Hitung mundur : {seconds}</h1>} ? 
                
            </div>
        )}
}

export default Timer