import React from "react"
import {Switch, Link, Route} from "react-router-dom"

import TableBuah from '../tugas11/table';
import Timer from '../tugas12/CountDown';
import Table from '../tugas13/table'
//import Clock from './tugas12/Jam';

const Routes = () =>{
    return (
        <>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/timer">Timer</Link>
            </li>
            <li>
              <Link to="/table">Table</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          
          <Route path="/timer" component={Timer}/>
          <Route path="/table">
            <Table />
          </Route>
          <Route path="/">
            <TableBuah />
          </Route>
        </Switch>
        </>
    )
}

export default Routes